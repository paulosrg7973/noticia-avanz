import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue';
import NewsPage from '@/views/NewsPage.vue';
import EditNews from '@/views/EditNews.vue';
import CreateNews from '@/views/CreateNews.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'index',
      component: HomeView
    },
    {
      path: '/news/:id',
      name: 'news',
      component: NewsPage
    },
    {
      path: '/news/create',
      name: 'create',
      component: CreateNews
    },
    {
      path: '/editor/:id?',
      name: 'editor',
      component: EditNews
    }
  ]
})

export default router
