# Teste Noticias Avanz

## Instalação:
Clone o projeto [noticias-avanz](https://gitlab.com/paulosrg7973/noticia-avanz.git)

    git clone https://gitlab.com/paulosrg7973/noticia-avanz.git

No diretorio noticias-frontend, copie o arquivo `.env.example` para `.env`

No diretorio noticias-backend, copie o arquivo `.env.example` para `.env`

### Docker:
Requisitos:
- Docker

1. Torne o script `run.sh` em executando o comando `chmod +x run.sh`
2. Execute o script `run.sh` ou abra o arquivo e execute os comandos manualmente

Observação: A instalação assume sua instalação do docker pode ser utilizada como usuário não root. Se não for o caso, execute o script como sudo `sudo run.sh` ou siga o [tutorial para configurar o docker como não usuário](https://docs.docker.com/engine/install/linux-postinstall/)

O backend estará disponível por padrão na porta 8083 e o frontend na porta 8085.

### Instalação direta
Requisitos:
- PHP >= 8.2
- Composer 2
- Node >= 18.0
- PostgreSQL >= 12.0

Configuração do backend:
1. Navegue até o diretório noticias-backend.
2. Modifique o arquivo `.env` do diretorio com os dados de conexão da sua instalação do PostgreSQL como explicado no final do documento.
3. Execute o comando `composer install`, `php artisan migrate` e `php artisan storage:link`
4. Inicie o backend com o comando `php artisan serve`

Configuração do frontend:
1. Navegue até o diretório noticias-frontend
2. Execute o comando `npm install`
3. Modifique o arquivo `.env` alterando a porta de acesso ao backend para a porta disponibilizada após a execução do comando `php artisan serve` (por padrão a porta 8000)
4. Execute o comando `npm run dev`

Tanto o backend quanto o front estarão disponíveis nas portas indicadas ao executar seus respectivos comandos no terminal

## Considerações finais:

Caso sinta necessidade, edite o arquivo `noticias-backend/.env` e insira o nome, usuario e senha do banco de dados de acordo com o exemplo
```
DB_DATABASE=noticias-avanz
DB_USERNAME=avanz
DB_PASSWORD=avanz
```
Substitua os valores com os de sua preferencia ou use os sugeridos no exemplo.
Caso mude as sugestões padrão, é necessario refletir essas mudanças no `docker-compose.yml` nos campos `POSTGRES_USER`, `POSTGRES_PASSWORD` e `POSTGRES_DB`
```
database:
    image: 'postgres:12-alpine'
    container_name: news_avanz_db
    environment:
        - POSTGRES_USER=avanz
        - POSTGRES_PASSWORD=avanz
        - POSTGRES_DB=noticias-avanz
    ports:
        - '8084:5432'
```