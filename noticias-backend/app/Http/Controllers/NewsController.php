<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsFormRequest;
use App\Models\News;
use Exception;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return News::select('id', 'title', 'lead', 'author', 'created_at')->get()->jsonSerialize();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(NewsFormRequest $request)
    {
        $newsData = $request->safe()->except('image');
        $newsData['body'] = htmlentities($newsData['body']);
        $image = $request->file('image')->store('', 'public');
        $newsData['image_path'] = $image;
        try
        {
            News::create($newsData);
        }
        catch(Exception $e)
        {
            return abort(500, $e->getMessage());
        }
        
        return response('', 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(News $news)
    {
        $newsData = $news->jsonSerialize();
        $newsData['body'] = html_entity_decode($newsData['body']);
        $newsData['image_path'] = asset('storage/'.$news['image_path']);
        return $newsData;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(News $news)
    {
        $newsData = $news->jsonSerialize();
        $newsData['body'] = html_entity_decode($newsData['body']);
        $newsData['image_path'] = asset('storage/'.$news['image_path']);
        return $newsData;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(NewsFormRequest $request, News $news)
    {
        $newsData = $request->safe()->except('image');
        Storage::delete($news->image_path);
        $image = $request->file('image');
        if($image)
        {
            $imagePath = $image->store('', 'public');
            $newsData['image_path'] = $imagePath;
        }
        $newsData['body'] = htmlentities($newsData['body']);
        
        try
        {
            $news->update($newsData);
        }
        catch(Exception $e)
        {
            return abort(500, $e->getMessage());
        }
        
        return response('', 201);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(News $news)
    {
        if($news->delete())
        {
            return response('', 200);
        }
        else
        {
            return abort(500);
        }
    }
}
