<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class NewsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'lead' => 'nullable',
            'body' => 'required',
            'author' => 'required',
            'image' => [Rule::requiredIf(fn () => !in_array('PUT', Route::getCurrentRoute()->methods)), 'mimes:jpg,png']
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'O título é obrigatório',
            'body.required' => 'O texto da notícia é obrigatório',
            'author.required' => 'O autor da notícia é obrigatório',
            'image.required' => 'É obrigatoria uma imagem',
            'image.mimes' => 'A imagem deve estar em JPEG ou PNG',
        ];
    }
}
