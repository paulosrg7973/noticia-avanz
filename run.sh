#!/bin/sh

docker-compose up -d
docker exec news_avanz_php chmod -R 777 storage
docker exec news_avanz_php composer install
docker exec news_avanz_php php artisan migrate
docker exec news_avanz_php php artisan storage:link
docker exec news_avanz_php php artisan key:generate